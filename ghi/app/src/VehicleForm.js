import React, { useEffect, useState } from 'react';


function VehicleForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [vehicleName, setVehicleName] = useState('');
    const [vehiclePicture, setVehiclePicture] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    let success = "alert alert-success d-none mb-0";


    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};
        data.name = vehicleName;
        data.picture_url = vehiclePicture;
        data.manufacturer_id = manufacturer;

        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            success = "alert alert-success mb-0";
            setVehicleName('');
            setVehiclePicture('');
            setManufacturer('');
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }


    const handleVehicleNameChange = (event) => {

        const value = event.target.value;
        setVehicleName(value);
    }

    const handleVehiclePictureChange = (event) => {

        const value = event.target.value;
        setVehiclePicture(value);
    }


    const handleManufacturerChange = (event) => {

        const value = event.target.value;
        setManufacturer(value);
    }


    useEffect(() => {
        fetchData();
    }, []);


    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (manufacturers.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-appointment-form">
                                <h1 className="card-title">Add a Vehicle Model</h1>
                                <p className="mb-3">
                                    Please choose a manufacturer
                                </p>
                                <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-4">
                                    <select value={manufacturer} onChange={handleManufacturerChange} name="manufacturer" id="technician" className={dropdownClasses} required>
                                        <option value="">Choose a manufacturer</option>
                                        {manufacturers.map(manufacturer => {
                                            return (
                                                <option value={manufacturer.id} key={manufacturer.id}>
                                                    {manufacturer.name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Vehicle Model
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="mb-3">
                                            <input value={vehicleName} onChange={handleVehicleNameChange} placeholder="Vehicle Model" type="text" id="name" name="name" className="form-control" />
                                        </div>
                                    </div>
                                    <p className="mb-3">
                                        Picture Url
                                    </p>
                                    <div className="mb-3">
                                        <input value={vehiclePicture} onChange={handleVehiclePictureChange} placeholder="URL" className="form-control" id="reason" name="picture"></input>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default VehicleForm;
