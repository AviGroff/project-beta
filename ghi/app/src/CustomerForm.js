import React, { useState } from 'react';


function CustomerForm() {
    const initialState = {
        name: "",
        address: "",
        phone: "",
    }
    const [formData, setFormData] = useState(initialState)

    const handleFormChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const customerUrl = 'http://localhost:8090/api/customers/'
        console.log(formData)
        const fetchConfig =
        {
            method: "POST",
            body: JSON.stringify(formData),

            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(customerUrl, fetchConfig)
        if (response.ok) {
            setFormData(initialState)
        }
    }

    return (
        <div className=" my-5 container">
            <div className="my-5">
                <div className="row">
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-salesperson-form">
                                    <h1 className="card-title">Add a Customer</h1>
                                    <p className="mb-3">
                                        Please provide customer information
                                    </p>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input onChange={handleFormChange} value={formData.name} placeholder="Name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input onChange={handleFormChange} value={formData.address} placeholder="Employee ID#" type="text" id="address" name="address" className="form-control" />
                                            <label htmlFor="address">Address</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input onChange={handleFormChange} value={formData.phone} placeholder="Phone #" type="text" id="phone" name="phone" className="form-control" />
                                            <label htmlFor="address">Phone #</label>
                                        </div>
                                        <button className="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CustomerForm
