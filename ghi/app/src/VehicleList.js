import React, { useEffect, useState } from 'react';


function VehicleList() {
  const [vehicles, setVehicles] = useState([]);


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setVehicles(data.models);
    }
  }


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <table className="table">
      <thead>
        <tr>
          <th>Vehicle Manufacturer</th>
          <th>Vehicle Model</th>
          <th>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Vehicle Picture</th>
        </tr>
      </thead>
      <tbody>
        {vehicles.map(model => {
          return (
            <tr key={model.id}>
              <td>{model.manufacturer.name}</td>
              <td>{model.name}</td>
              <td><img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src={model.picture_url} /></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}

export default VehicleList;
