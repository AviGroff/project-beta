import React, { useEffect, useState } from 'react';


function AutomobileList() {
  const [Automobiles, setAutomobiles] = useState([]);


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }


  useEffect(() => {
    fetchData();
  }, []);


  return (
    <table className="table">
      <thead>
        <tr>
          <th>Year</th>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>VIN</th>
          <th>Color</th>
        </tr>
      </thead>
      <tbody>
        {Automobiles.map(auto => {
          return (
            <tr key={auto.id}>
              <td>{auto.year}</td>
              <td>{auto.model.manufacturer.name}</td>
              <td>{auto.model.name}</td>
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}

export default AutomobileList;
