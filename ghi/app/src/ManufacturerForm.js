import React, { useState } from 'react';


function ManufacturerForm() {
    const [name, setName] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;

        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            setName('');
        }
    }


    const handleNameChange = (event) => {

        const value = event.target.value;
        setName(value);
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-manufacturer-form">
                                <h1 className="card-title">Add a Manufacturer</h1>
                                <div className="row">
                                    <div className="col">
                                        <div className="mb-3">
                                            <input value={name} onChange={handleNameChange} required placeholder="Manufacturer Name" type="text" id="manufacturerName" name="manufacturerName" className="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default ManufacturerForm;
