import React, { useEffect, useState } from 'react';


function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [filterTerm, setFilterTerm] = useState(" ")

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);


    const handleFilterChange = (event) => {
        setFilterTerm(event.target.value.toLowerCase());
    }


    function getFiltered() {
        if (filterTerm === " ") {
            return appointments
        }
        else {
            return appointments.filter(appointment => appointment.vin.toLowerCase().includes(filterTerm))
        }
    }


    return (
        <>
            <input onChange={handleFilterChange} required placeholder="Vin" type="text" id="name" name="name" className="mt-3 form-control" />
            <table className="table">
                <thead>
                    <tr>
                        <th>Finished</th>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {getFiltered().map(appointment => {

                        let date = appointment.date_time.substring(0, 10)
                        let hour = Number(appointment.date_time.substring(11, 13))
                        let minute = appointment.date_time.substring(14, 16)
                        let meridiem = "am";
                        if (hour > 12) {
                            hour -= 12;
                            hour = String(hour);
                            meridiem = "pm";
                        }
                        else if (hour == 0) {
                            hour = "12"
                        }

                        let time = hour + ":" + minute + meridiem;
                        return (
                            <tr key={appointment.id}>
                                <td>{String(appointment.finished)}</td>
                                <td>{appointment.vin}</td>
                                <td>{String(appointment.vip)}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{date}</td>
                                <td>{time}</td>
                                <td>{appointment.technician.name}</td>
                                <td>{appointment.reason}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ServiceHistory;
