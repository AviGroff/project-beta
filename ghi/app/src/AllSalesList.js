import React, { useEffect, useState } from 'react';


function AllSalesList() {
    const [sales, setSales] = useState([]);


    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Employee ID#</th>
                    <th>Purchaser</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={sales.all}>
                            <td>{sale.salesperson.name}</td>
                            <td>{sale.salesperson.employee_id}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default AllSalesList
