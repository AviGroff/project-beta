import React, { useEffect, useState } from 'react';


function AppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [customerName, setCustomerName] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [technician, setTechnician] = useState('');
    const [dateTime, setDateTime] = useState('');

    let success = "alert alert-success d-none mb-0";

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.vin = vin;
        data.customer_name = customerName;
        data.date_time = dateTime;
        data.technician = technician;
        data.reason = reason;

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            success = "alert alert-success mb-0";
            setCustomerName('');
            setReason('');
            setTechnician('');
            setDateTime('');
            setVin('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }


    const handleVinChange = (event) => {

        const value = event.target.value;
        setVin(value);
    }


    const handleCustomerNameChange = (event) => {

        const value = event.target.value;
        setCustomerName(value);
    }


    const handleReasonChange = (event) => {

        const value = event.target.value;
        setReason(value);
    }


    const handleDateTimeChange = (event) => {

        const value = event.target.value;
        setDateTime(value);
    }


    const handleTechnicianChange = (event) => {

        const value = event.target.value;
        setTechnician(value);
    }


    useEffect(() => {
        fetchData();
    }, []);


    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (technicians.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-appointment-form">
                                <h1 className="card-title">It's Appointment Time!</h1>
                                <p className="mb-3">
                                    Please choose a technician
                                </p>
                                <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select value={technician} onChange={handleTechnicianChange} name="technician" id="technician" className={dropdownClasses} required>
                                        <option value="">Choose a technician</option>
                                        {technicians.map(technician => {
                                            return (
                                                <option value={technician.employee_number} key={technician.employee_number}>
                                                    {technician.name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    SHOW ME WHAT YOU GOT
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input value={customerName} onChange={handleCustomerNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlform="name">Customer Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input value={dateTime} onChange={handleDateTimeChange} required placeholder="datetime" type="datetime-local" id="date" name="date" className="form-control" />
                                            <label htmlform="email">Date and Time</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input value={vin} onChange={handleVinChange} required placeholder="vin" type="text" id="vin" name="vin" className="form-control" />
                                            <label htmlform="email">VIN</label>
                                        </div>
                                    </div>
                                    <div className="mb-3">
                                        <div className="from-floating-mb4">
                                            <p>Reason</p>
                                            <textarea value={reason} onChange={handleReasonChange} className="form-control" id="reason" rows="3" name="reason"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AppointmentForm;
