import React, { useEffect, useState } from 'react';


function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('');

    let success = "alert alert-success d-none mb-0";

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.color = color;
        data.year = year;
        data.vin = vin
        data.model_id = model;

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            success = "alert alert-success mb-0";
            setColor('');
            setVin('');
            setYear('');
            setModel('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    const handleColorChange = (event) => {

        const value = event.target.value;
        setColor(value);
    }


    const handleYearChange = (event) => {

        const value = event.target.value;
        setYear(value);
    }


    const handleVinChange = (event) => {

        const value = event.target.value;
        setVin(value);
    }


    const handleModelChange = (event) => {

        const value = event.target.value;
        setModel(value);
    }

    useEffect(() => {
        fetchData();
    }, []);

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (models.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-appointment-form">
                                <h1 className="card-title">Add an Automobile to Inventory</h1>
                                <p className="mb-3">
                                    Please choose a model
                                </p>
                                <div className={spinnerClasses} id="loading-conference-spinner">
                                    <div className="spinner-grow text-secondary" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select value={model} onChange={handleModelChange} name="model" id="model" className={dropdownClasses} required>
                                        <option value="">Choose a model</option>
                                        {models.map(model => {
                                            return (
                                                <option value={model.id} key={model.id}>
                                                    {model.name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Automobile details
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="mb-3">
                                            <input value={color} onChange={handleColorChange} required placeholder="Color" type="text" id="name" name="name" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="mb-3">
                                            <input value={year} onChange={handleYearChange} required placeholder="Year" type="text" id="name" name="name" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="mb-3">
                                            <input value={vin} onChange={handleVinChange} required placeholder="VIN" type="text" id="name" name="name" className="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AutomobileForm;
