import React, { useEffect, useState } from 'react';


function EmployeeSalesList() {
    const [sales, setSales] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('')


    const fetchData1 = async () => {
        const url = 'http://localhost:8090/api/sales/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }

    }


    const fetchData2 = async () => {
        const url = 'http://localhost:8090/api/salespersons/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons);
        }
    }


    const handleFormChange = async (event) => {
        event.preventDefault();

        setSalesperson(event.target.value)
    }


    useEffect(() => {
        fetchData1();
    }, []);


    useEffect(() => {
        fetchData2();
    }, []);


    let dropdownClasses = 'form-select';
    let employee_sales = sales.filter(sale => sale.salesperson.name === salesperson)

    return (
        <div>
            <br></br>
            <div className="mb-3">
                <select onChange={handleFormChange} value={salesperson} name="salesperson" id="salesperson" className={dropdownClasses} required>
                    <option value="">Select a Salesperson</option>
                    {salespersons.map(salesperson => {
                        return (
                            <option value={salesperson.name} key={salesperson.name}>
                                {salesperson.name}
                            </option>
                        )
                    })}
                </select>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {employee_sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default EmployeeSalesList
