import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentsList';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SaleRecordForm from './SaleRecordForm';
import AllSalesList from './AllSalesList';
import EmployeeSalesList from './EmployeeSalesList';
import ServiceHistory from './ServiceHistory';
import ManufacturersList from './ManufacturersList';
import VehicleList from './VehicleList';
import AutomobileList from './AutomobileList';
import ManufacturerForm from './ManufacturerForm';
import VehicleForm from './VehicleForm';
import AutomobileForm from './AutomobileForm';
import "./index.css"


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="/appointments" element={<AppointmentsList />} />
          <Route path="/salespersons/new" element={<SalesPersonForm />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales/new" element={<SaleRecordForm />} />
          <Route path="/sales/all" element={<AllSalesList />} />
          <Route path="/sales/salesperson" element={<EmployeeSalesList />} />
          <Route path="/appointments/history" element={<ServiceHistory />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/models" element={<VehicleList />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/models/new" element={<VehicleForm />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
