import React, { useState } from 'react';


function TechnicianForm() {
    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.employee_number = employeeNumber;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setEmployeeNumber('');
        }
    }


    const handleNameChange = (event) => {

        const value = event.target.value;
        setName(value);
    }


    const handleEmployeeNumberChange = (event) => {

        const value = event.target.value;
        setEmployeeNumber(value);
    }

    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form onSubmit={handleSubmit} id="create-technician-form">
                                <h1 className="card-title">Add a Technician</h1>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input value={name} onChange={handleNameChange} required placeholder="technician name" type="text" id="technicianName" name="technicianName" className="form-control" />
                                            <label htmlform="name">Technician Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input value={employeeNumber} onChange={handleEmployeeNumberChange} required placeholder="Employee number" type="number" id="employeeNumber" name="employeeNumber" className="form-control" />
                                            <label htmlform="eid">Employee Number</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
