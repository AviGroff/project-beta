import React, { useState } from 'react';


function SalesPersonForm() {
    const initialState = {
        name: "",
        employee_id: "",
    }
    const [formData, setFormData] = useState(initialState)

    const handleFormChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const salesPersonUrl = 'http://localhost:8090/api/salespersons/'

        const fetchConfig =
        {
            method: "POST",
            body: JSON.stringify(formData),

            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(salesPersonUrl, fetchConfig)
        if (response.ok) {
            await response.json()
            setFormData(initialState)
        }

    }

    return (
        <div className=" my-5 container">
            <div className="my-5">
                <div className="row">
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-salesperson-form">
                                    <h1 className="card-title">Add a Salesperson</h1>
                                    <p className="mb-3">
                                        Please provide salesperson information
                                    </p>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input onChange={handleFormChange} value={formData.name} placeholder="Name" type="text" id="name" name="name" className="form-control" />
                                            <label htmlFor="name">Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-4">
                                            <input onChange={handleFormChange} value={formData.employee_id} placeholder="Employee ID#" type="text" id="employee_id" name="employee_id" className="form-control" />
                                            <label htmlFor="employee_id">Employee ID#</label>
                                        </div>
                                        <button className="btn btn-lg btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SalesPersonForm
