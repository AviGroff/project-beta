import React, { useEffect, useState } from 'react';


function AppointmentsList() {
  const [appointments, setAppointments] = useState([]);


  const fetchData = async () => {
    const url = 'http://localhost:8080/api/appointments/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const deleteAppointment = async (event) => {
    const appointmentId = event.target.id
    // create an empty JSON object

    const appointmentUrl = `http://localhost:8080/api/appointments/${appointmentId}`;
    const fetchConfig = {
      method: "delete",
      headers: {
        'Content-Type': 'application/json',
      },
    };

    await fetch(appointmentUrl, fetchConfig);
  }

  const updateAppointment = async (event) => {
    const appointmentId = event.target.id
    const data = {};

    data.finished = true;

    const appointmentUrl = `http://localhost:8080/api/appointments/${appointmentId}/`;
    const fetchConfig = {
      method: "put",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    await fetch(appointmentUrl, fetchConfig);
  }


  return (
    <table className="table">
      <thead>
        <tr>
          <th>VIN</th>
          <th>VIP</th>
          <th>Customer name</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.filter(appointment => appointment.finished == false).map(appointment => {

          let date = appointment.date_time.substring(0, 10)
          let hour = Number(appointment.date_time.substring(11, 13))
          let minute = appointment.date_time.substring(14, 16)
          let meridiem = "am";
          if (hour > 12) {
            hour -= 12;
            hour = String(hour);
            meridiem = "pm";
          }
          else if (hour == 0) {
            hour = "12"
          }

          let time = hour + ":" + minute + meridiem;
          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{String(appointment.vip)}</td>
              <td>{appointment.customer_name}</td>
              <td>{date}</td>
              <td>{time}</td>
              <td>{appointment.technician.name}</td>
              <td>{appointment.reason}</td>
              <td><button className="btn btn-danger" id={appointment.id} onClick={deleteAppointment}>Cancel</button><button className="btn btn-success" id={appointment.id} onClick={updateAppointment}>Finish</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  )
}

export default AppointmentsList;
