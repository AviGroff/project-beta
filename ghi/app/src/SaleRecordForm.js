import React, { useEffect, useState } from 'react';


function SaleRecordForm() {
    const [formData, setFormData] = useState({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
    })

    const [automobiles, setAutomobiles] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);

    const handleFormChange = (event) => {
        setFormData({
            ...formData,
            [event.target.name]: event.target.value
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/'

        const fetchConfig =
        {
            method: "POST",
            body: JSON.stringify(formData),

            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(salesUrl, fetchConfig)
        if (response.ok) {
            await response.json()
            setFormData({
                automobile: "",
                salesperson: "",
                customer: "",
                price: "",
            });
        }
    }

    const fetchData1 = async () => {
        const url1 = 'http://localhost:8090/api/salespersons/';

        const response = await fetch(url1);

        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons);
            ;
        }
    }


    const fetchData2 = async () => {
        const url2 = 'http://localhost:8090/api/customers/';

        const response = await fetch(url2);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    const fetchData3 = async () => {
        const url3 = 'http://localhost:8090/api/automobiles/';

        const response = await fetch(url3);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.automobiles.filter(automobile => automobile.is_sold == false));
        }
    }


    useEffect(() => {
        fetchData1();
    }, []);


    useEffect(() => {
        fetchData2();
    }, []);


    useEffect(() => {
        fetchData3();
    }, []);


    let dropdownClasses = 'form-select';

    return (
        <div className=" my-5 container">
            <div className="my-5">
                <div className="row">
                    <div className="col">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-sale-form">
                                    <h1 className="card-title">SELL SELL SELL!</h1>
                                    <div className="mb-3">
                                        <select onChange={handleFormChange} value={formData.automobile} name="automobile" id="automobile" className={dropdownClasses} required>
                                            <option value="">Choose an automobile</option>
                                            {automobiles.map(automobile => {
                                                return (
                                                    <option key={automobile.vin}>
                                                        {automobile.vin}
                                                    </option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={handleFormChange} value={formData.salesperson} name="salesperson" id="salesperson" className={dropdownClasses} required>
                                            <option value="">Choose a salesperson</option>
                                            {salespersons.map(salesperson => {
                                                return (
                                                    <option key={salesperson.name}>
                                                        {salesperson.name}
                                                    </option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <select onChange={handleFormChange} value={formData.customer} name="customer" id="customer" className={dropdownClasses} required>
                                            <option value="">Choose a customer</option>
                                            {customers.map(customer => {
                                                return (
                                                    <option key={customer.name}>
                                                        {customer.name}
                                                    </option>
                                                )
                                            })}
                                        </select>
                                    </div>
                                    <div className="mb-3">
                                        <input onChange={handleFormChange} value={formData.price} placeholder="Price" type="text" id="price" name="price" className="form-control" />
                                    </div>
                                    <button className="btn btn-lg btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default SaleRecordForm
