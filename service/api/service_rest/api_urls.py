from django.urls import path
from .api_views import api_list_appointments, api_list_technicians, api_appointments_detail


urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", api_appointments_detail, name="api_appointments_detail"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
]
