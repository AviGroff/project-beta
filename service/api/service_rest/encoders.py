from common.json import ModelEncoder
from .models import Technician, Appointment

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["name"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["name", "employee_number"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "vip",
        "customer_name",
        "date_time",
        "technician",
        "reason",
        "finished",
    ]
    encoders = {"technician": TechnicianListEncoder()}
