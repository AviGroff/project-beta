from django.db import models

# Create your models here.
# We'll use automobile VO to store all vins that are in inventory
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)


class Technician(models.Model):
    name = models.CharField(max_length=40)
    employee_number = models.PositiveIntegerField()


class Appointment(models.Model):
    finished = models.BooleanField(default=False)
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)
    customer_name = models.CharField(max_length=40)
    date_time = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name = "appointment",
        on_delete=models.CASCADE,
    )

    reason = models.TextField()
