from django.urls import path

from .views import (
    api_list_sales,
    api_list_salespersons,
    api_list_customers,
    api_list_automobiles
)

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("salespersons/", api_list_salespersons, name="api_list_salespersons"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("automobiles/", api_list_automobiles, name="api_list_automobiles")
]
