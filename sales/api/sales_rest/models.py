from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=20)
    is_sold = models.BooleanField(default=False)

class SalesPerson(models.Model):
    name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=16)

class Customer(models.Model):
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)

class Sale(models.Model):
    price = models.CharField(max_length=24)
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sale",
        on_delete=models.CASCADE,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale",
        on_delete=models.CASCADE
    )
