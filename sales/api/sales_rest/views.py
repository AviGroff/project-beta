from django.http import JsonResponse
from .models import Sale
import json
from common.json import ModelEncoder
from .encoders import CustomerEncoder, SalesPersonEncoder, SaleDetailEncoder, AutomobileVOEncoder
from .models import Sale, SalesPerson, Customer, AutomobileVO
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):

    if request.method == "GET":
        salespersons = SalesPerson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalesPersonEncoder,
        )

    if request.method == "POST":
        content = json.loads(request.body)
        salesperson = SalesPerson.objects.create(name=content["name"], employee_id=content["employee_id"])
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):

    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )

    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(name=content["name"], address=content["address"], phone=content["phone"])
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):

    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleDetailEncoder,
        )

    if request.method == "POST":
        content = json.loads(request.body)
        if AutomobileVO.objects.get(vin=content["automobile"]).is_sold == True:
            return JsonResponse({"Error": "This car has already been sold."}, status=400)
        if AutomobileVO.objects.get(vin=content["automobile"]).is_sold == False:
            content["salesperson"] = SalesPerson.objects.get(name=content["salesperson"])
            content["customer"] = Customer.objects.get(name=content["customer"])
            content["automobile"] = AutomobileVO.objects.get(vin=content["automobile"])
            sale = Sale.objects.create(salesperson=content["salesperson"], customer=content["customer"], automobile=content["automobile"], price=content["price"])
            AutomobileVO.objects.filter(vin=content["automobile"].vin).update(is_sold=True)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def api_list_automobiles(request):

    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )
