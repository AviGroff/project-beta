# CarCar

Team:

* Person 1 - Which microservice?
Avi Groff - Automobile Service

* Person 2 - Which microservice?
Eric Mills - Automobile Sales
## Design

Bounded Contexts:
    Service
        Models
            Appointment - Keep track of Service appointments
            Technician - Keep track of technicians on payroll
            AutomobileVO - Automobile Data from Inventory Automobile Model

    Inventory
        Models
            Manufacturer
            VehicleModel
            Automobile

    Sales
        Models
            SalesPerson - Keep track of sales people on payroll
            Customer - Keep track of customers buying cars
            Sale - Keep track of automobile sales
            AutomobileVO - Automobile data from inventory automobile model

## Service microservice

Models:
AutomobileVO
    We need a way to grab the automobile data from the inventory microservice.
    The only relevant automobile data for my use case is the vin for checking
    to see if the vehicle is in our inventory automobile model to apply a
    vip designator to that service appointment.

Technician
    We need a way to keep track of our technicians along with their unique
    employee numbers. While there may be multiple technicians with the same name
    there will not be multiple technicians with the same employee number.

Appointment
    We need a way to keep track of our service appointments.
    Relevant Data
        Finished - Designates wether the appointment has been completed yet
        Vin - The automobile vin associated with the appointment
        Vip - Whether the vehicle was in our inventory at some point in time
        Customer_name - The name associated with the appointment
        date_time - The date and time of the appointment
        technician - the technician instance associated with this appointment
        reason - A reason for this service appointment

    I chose to add vip in order to update the value of vip within our backend
    This variable will check to see if the vin exists within automobileVO
    if the vin exists there then the vip field will update to true, if not
    it will maintain its default state of false.

Frontend
    Added the ability to check service history by vin by applying onChange to
    the input field. No need for a submit button to apply the search. Simply
    input the vin (not case sensitive) and the service historys will filter
    to only contain the relevant vin.


## Sales microservice

Models:
AutomobileVO -> We need something to sell, but the only properties we require for sales purposes are the VIN and whether it has already been sold so it can't accidentally be sold again. The get_vin() and poll() functions in poller.py "talk" to the inventory microservice to populate our sales inventory accordingly.
Salesperson -> Someone's gotta sell the cars and we are woke enough to be gender neutral 'round these parts.
Customer -> Someone's gotta buy the cars.
Sale -> This one is tricky, because each sale has an automobile(by VIN), a salesperson, a customer, and a price.
We utilized the AutomobileVO and multiple foreignkeys to "connect" the other models to Sale.
